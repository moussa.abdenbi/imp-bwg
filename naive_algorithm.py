import sys
import time
import random
from collections import deque
from itertools import combinations
from copy import deepcopy
from utilities_functions import *

INFINI = - sys.maxint - 1

def naive_algorithm(D, i):
    d = INFINI
    for E in combinations(D.keys(), i):
        I = induced_graph(D, list(E))
        if is_acyclic(I):
            d_temp = delta(I)
            if d_temp > d:
                d = d_temp
        # Maximal achievable delta for i vertices
        if d == i - 2:
            break
    return d

path = sys.argv[1]
D = load_graph(path)
for i in range(3, len(D)):
    start = time.time()
    d = naive_algorithm(D, i)
    end = time.time()
    print(str(i) + ',' + str(d) + ',' + str(end -start))
