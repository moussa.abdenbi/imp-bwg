import sys
import time
import random
from collections import deque
from itertools import combinations
from copy import deepcopy
from utilities_functions import *

INFINI = - sys.maxint - 1

def initial_solution(D, i):
    V = D.keys()
    random.shuffle(V)
    for E in combinations(V, i):
        I = induced_graph(D, list(E))
        if is_acyclic(I):
            return I

def neighbors_vertices(D, I):
    diff = list(set(D.keys()).difference(set(I.keys())))
    M = set()
    for u in diff:
        for v, adj in D.items():
            if v in I.keys() and u in adj:
                M.add(u)
    return list(M)

def neighborhood(D, I, u):
    for v in I.keys():
        V_prim = deepcopy(I.keys())
        V_prim.remove(v)
        V_prim.append(u)
        I_prim = induced_graph(D, list(set(V_prim)))
        if is_acyclic(I_prim):
            yield I_prim, v

def tabu_search(D, I):
    I_prim = deepcopy(I)
    B = deepcopy(I)
    T = deque([], maxlen=2)
    NB_ITERATIONS = 3 * len(D)
    i = 0
    keep_looping = True
    while keep_looping:
        exit = False
        N = neighbors_vertices(D, I_prim)
        if not N:
            break
        for u in N:
            if not u in T:
                for I_pp, v in neighborhood(D, I_prim, u):
                    if delta(I_prim) < delta(I_pp):
                        T.append(v)
                        I_prim = deepcopy(I_pp)
                        if delta(B) < delta(I_pp):
                            B = deepcopy(I_pp)
                        exit = True
                        break
                if exit:
                    break
                T.append(u)
        v = T[0]
        for I_pp, w in neighborhood(D, I_prim, v):
            if delta(B) < delta(I_pp):
                B = deepcopy(I_pp)
        i += 1
        keep_looping = B != I_prim and i < NB_ITERATIONS
    return delta(B)

path = sys.argv[1]
D = load_graph(path)
for i in range(3, len(D)):
    I = initial_solution(D, i)
    if I == None:
        print(str(i) + ',-inf,0')
    else:
        start = time.time()
        d = tabu_search(D, I)
        end = time.time()
        print(str(i) + ',' + str(d) + ',' + str(end -start))
